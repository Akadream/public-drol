/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.scenes;

import drol.entities.Enemy;
import drol.entities.Player;
import drol.entities.PlayerNet;
import java.util.List;
import javax.swing.*;

/**
 *
 * @author Akadream
 */
public class Window extends JFrame {

    /**
     *
     * @param width
     * @param height
     * @param nbPlayers
     */
    public Window(int width, int height) {
    // Window options
    this.setTitle("Drol - By Guillaume and Oceane");
    this.setSize(width + 6, height + 32);
    this.setResizable(false);
    this.setLocationRelativeTo(null);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    // Set this frame visible
    this.setVisible(true);
  }

    /**
     *
     * @param enemy
     */
    public void addEnemyListener(Enemy enemy) {
    this.addKeyListener(enemy);
  }

    /**
     *
     * @param players
     */
    public void addPlayersListener(List<Player> players) {
    // Key listening
    players.forEach((player) -> {
        this.addKeyListener(player);
    });
  }

    /**
     *
     * @param players
     */
    public void addPlayersNetListener(List<PlayerNet> players) {
    // Key listening
    players.forEach((player) -> {
        this.addKeyListener(player);
    });
  }
}