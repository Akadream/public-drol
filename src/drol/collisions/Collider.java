/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.collisions;

/**
 *
 * @author Akadream
 */
/**
 * Relative position of the parent
 */
public class Collider {
  private int x;
  private int y;
  private int width;
  private int height;

  public Collider(int x, int y, int width, int height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  public int getRight() {
    return x + width;
  }

  public int getBottom() {
    return y + height;
  }

  public int getMiddleX() {
    return (int)(x + width / 2);
  }

  public int getMiddleY() {
    return (int)(y + height / 2);
  }

  public void setX(int x) {
    this.x = x;
  }

  public void setY(int y) {
    this.y = y;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public void set(int x, int y, int width, int height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }
}
