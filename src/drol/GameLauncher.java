/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol;

import drol.scenes.Window;

/**
 *
 * @author Akadream
 */
public class GameLauncher {
  public static void main(String[] args) {
    // Create the game
    Game game = new Game(new Window(Settings.WORLD_WIDTH, Settings.WORLD_HEIGHT));

    // Start the game
    game.start();
  }
}