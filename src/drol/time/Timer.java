/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.time;

import java.util.Date;

/**
 *
 * @author Akadream
 */
public class Timer {
  private long startingTimer;
  private long elapsedTime = 0L;

  public Timer() {
    startingTimer = System.currentTimeMillis();
  }

  public Timer(long time) {
    startingTimer = time;
  }

  public long getElapasedTime() {
    elapsedTime = (new Date()).getTime() - startingTimer;
    return elapsedTime;
  }

  public void reset() {
    startingTimer = System.currentTimeMillis();
    elapsedTime = 0L;
  }
}