/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.net;

/**
 *
 * @author Akadream
 */
public enum PacketTypes {
    INVALID(-1), LOGIN(00), DISCONNECT(01), MOVE(02), HUMAN(03), ADDMONSTERS(04), REMOVEMONSTERS(05), LASER(06);

    private int packetId;

    private PacketTypes(int packetId) {
        this.packetId = packetId;
    }

    public int getId() {
        return packetId;
    }
}