/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.net;

/**
 *
 * @author Akadream
 */
public class PacketHuman extends Packet {
  private float x, y;

  public PacketHuman(byte[] data) {
    super(03);
    String[] dataArray = readData(data).split(",");
    this.x = Float.parseFloat(dataArray[0]);
    this.y = Float.parseFloat(dataArray[1]);
  }

  public PacketHuman(float x, float y) {
    super(03);
    this.x = x;
    this.y = y;
  }

  @Override
  public void writeData(GameClient client) {
    client.sendData(getData());
  }

  @Override
  public void writeData(GameServer server) {
    server.sendDataToAllClients(getData());
  }

  @Override
  public byte[] getData() {
    return ("03" + getX() + "," + getY()).getBytes();
  }

  public float getX() {
    return this.x;
  }

  public float getY() {
    return this.y;
  }
}

