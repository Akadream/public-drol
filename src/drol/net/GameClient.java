/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.net;

import drol.Game;
import drol.entities.PlayerNet;
import java.io.IOException;
import java.net.UnknownHostException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 *
 * @author Akadream
 */
public class GameClient extends Thread {
  private InetAddress ipAddress;
  private DatagramSocket socket;
  private Game game;
  private boolean running = true;

  public GameClient(Game game, String ipAddress) {
    this.game = game;

    try {
      this.socket = new DatagramSocket();
      this.ipAddress = InetAddress.getByName(ipAddress);
    }
    catch(SocketException | UnknownHostException e) {
    }
  }

  @Override
  public void run() {
    System.out.println("Client running...");
    while(running) {
      byte[] data = new byte[1024];
      DatagramPacket packet = new DatagramPacket(data, data.length);

      try {
        socket.receive(packet);
      }
      catch(IOException e) {
      }

      this.parsePacket(packet.getData(), packet.getAddress(), packet.getPort());
    }
  }

  private void parsePacket(byte[] data, InetAddress address, int port) {
    String message = new String(data).trim();
    PacketTypes type = Packet.lookupPacket(message.substring(0, 2));
    Packet packet = null;
    switch (type) {
    default:
    case INVALID:
        break;
    case LOGIN:
        packet = new PacketLogin(data);
        handleLogin((PacketLogin) packet, address, port);
        break;
    case DISCONNECT:
        packet = new PacketDisconnect(data);
        System.out.println("[" + address.getHostAddress() + ":" + port + "] "
                + ((PacketDisconnect) packet).getUsername() + " has left the world...");
        game.removePlayer(((PacketDisconnect) packet).getUsername());
        break;
    case MOVE:
        packet = new PacketMove(data);
        handleMove((PacketMove) packet);
        break;
      case HUMAN:
        packet = new PacketHuman(data);
        setHuman((PacketHuman) packet);
        break;
      case ADDMONSTERS:
        break;
      case REMOVEMONSTERS:
        break;
    }
  }

  public void sendData(byte[] data) {
    if (!game.isApplet) {
        DatagramPacket packet = new DatagramPacket(data, data.length, ipAddress, 1331);
        try {
            socket.send(packet);
        } catch (IOException e) {
        }
    }
  }

  private void handleLogin(PacketLogin packet, InetAddress address, int port) {
    System.out.println("[" + address.getHostAddress() + ":" + port + "] " + packet.getUsername()
            + " has joined the game...");
    PlayerNet player = new PlayerNet(1, packet.getUsername(), packet.getX(), packet.getY(), game.getMap(), address, port);
    game.addPlayerNet(player);
  }

  private void handleMove(PacketMove packet) {
    this.game.movePlayer(packet.getUsername(), packet.getX(), packet.getY(), packet.getDirection());
  }

  private void setHuman(PacketHuman packet) {
    this.game.setHuman(packet.getX(), packet.getY());
  }
}