/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.net;

import drol.Game;
import drol.entities.Enemy;
import drol.entities.Human;
import drol.entities.PlayerNet;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Akadream
 */
public class GameServer extends Thread {
  private DatagramSocket socket;
  private Game game;
  private Human human;
  private List<PlayerNet> connectedPlayers = new ArrayList<>();
  private List<Enemy> enemies = new ArrayList<>();

  public GameServer(Game game) {
    this.game = game;
    this.human = game.currentHuman;

    try {
      this.socket = new DatagramSocket(1331);
    }
    catch(SocketException e) {
    }
  }

  @Override
  public void run() {
    System.out.println("Server running...");
    while(true) {
      byte[] data = new byte[1024];
      DatagramPacket packet = new DatagramPacket(data, data.length);

      try {
        socket.receive(packet);
      }
      catch(IOException e) {
      }

      this.parsePacket(packet.getData(), packet.getAddress(), packet.getPort());
    }
  }

  private void parsePacket(byte[] data, InetAddress address, int port) {
    String message = new String(data).trim();
    PacketTypes type = Packet.lookupPacket(message.substring(0, 2));
    Packet packet = null;

    switch(type) {
      default:
      case INVALID:
        break;
      case LOGIN:
        packet = new PacketLogin(data);
        System.out.println("[" + address.getHostAddress() + " : " + port + "] " + ((PacketLogin) packet).getUsername() + " has connected...");
        // Give game infos to the new player here
        PlayerNet player = new PlayerNet(1, ((PacketLogin) packet).getUsername(), game.getMap(), address, port);
        this.addConnection(player, (PacketLogin) packet);
        break;
      case DISCONNECT:
        packet = new PacketDisconnect(data);
        System.out.println("[" + address.getHostAddress() + " : " + port + "] " + ((PacketDisconnect) packet).getUsername() + " has left...");
        this.removeConnection((PacketDisconnect) packet);
        break;
      case MOVE:
        packet = new PacketMove(data);
        this.handleMove(((PacketMove) packet));
        break;
      case HUMAN:
        packet = new PacketHuman(data);
        this.setHuman(((PacketHuman) packet));
        break;
      case ADDMONSTERS:
        break;
      case REMOVEMONSTERS:
        break;
    }
  }

  public void addConnection(PlayerNet player, PacketLogin packet) {
    boolean alreadyConnected = false;
    for(PlayerNet p : this.connectedPlayers) {
      if(player.getUsername().equalsIgnoreCase(p.getUsername())) {
        if(p.ipAddress == null) {
          p.ipAddress = player.ipAddress;
        }

        if(p.port == -1) {
          p.port = player.port;
        }

        alreadyConnected = true;
      }
      else {
        sendData(packet.getData(), p.ipAddress, p.port);
        packet = new PacketLogin(p.getUsername(), p.getX(), p.getY());
        sendData(packet.getData(), player.ipAddress, player.port);
      }
    }

    if(!alreadyConnected) {
      this.connectedPlayers.add(player);
    }
  }

  public void addHuman(Human human, PacketHuman packet) {
    //sendData(packet.getData(), )
  }

  public void removeConnection(PacketDisconnect packet) {
    this.connectedPlayers.remove(getPlayerNetIndex(packet.getUsername()));
    packet.writeData(this);
  }

  public PlayerNet getPlayerNet(String username) {
    for(PlayerNet player : this.connectedPlayers) {
      if(player.getUsername().equals(username)) {
        return player;
      }
    }

    return null;
  }

  public int getPlayerNetIndex(String username) {
    int index = 0;

    for(PlayerNet player : this.connectedPlayers) {
      if(player.getUsername().equals(username)) {
        break;
      }
      index++;
    }

    return index;
  }

  public void sendData(byte[] data, InetAddress ipAddress, int port) {
    if(!game.isApplet) {
      DatagramPacket packet = new DatagramPacket(data, data.length, ipAddress, port);
      try {
        this.socket.send(packet);
      }
      catch(IOException e) {
      }
    }
  }

  public void sendDataToAllClients(byte[] data) {
      connectedPlayers.forEach((player) -> {
          sendData(data, player.ipAddress, player.port);
      });
  }

  private void handleMove(PacketMove packet) {
    if(getPlayerNet(packet.getUsername()) != null) {
      int index = getPlayerNetIndex(packet.getUsername());
      PlayerNet player = this.connectedPlayers.get(index);
      player.setX(packet.getX());
      player.setY(packet.getY());
      player.setDirection(packet.getDirection());
      packet.writeData(this);
    }
  }

  private void setHuman(PacketHuman packet) {
    if(human != null) {
      System.out.println("Test");
      human.setX(packet.getX());
      human.setY(packet.getY());
      packet.writeData(this);
    }
  }
}
