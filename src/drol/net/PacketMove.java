/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.net;

import drol.entities.Directions;

/**
 *
 * @author Akadream
 */
public class PacketMove extends Packet {
    private final String username;
    private final float x, y;
    private final Directions direction;

    public PacketMove(byte[] data) {
        super(02);
        String[] dataArray = readData(data).split(",");
        this.username = dataArray[0];
        this.x = Float.parseFloat(dataArray[1]);
        this.y = Float.parseFloat(dataArray[2]);
        this.direction = Directions.valueOf(dataArray[3]);
    }

    public PacketMove(String username, float x, float y, Directions direction) {
        super(02);
        this.username = username;
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    @Override
    public void writeData(GameClient client) {
        client.sendData(getData());
    }

    @Override
    public void writeData(GameServer server) {
        server.sendDataToAllClients(getData());
    }

    @Override
    public byte[] getData() {
        return ("02" + this.username + "," + this.x + "," + this.y + "," + this.direction).getBytes();

    }

    public String getUsername() {
        return username;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public Directions getDirection() {
      return this.direction;
    }
}

