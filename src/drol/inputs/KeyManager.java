/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.inputs;

import java.util.Hashtable;

/**
 *
 * @author Akadream
 */
public class KeyManager {
  private final Hashtable<String, Integer> keys;

  public KeyManager() {
    keys = new Hashtable<>();
  }

  public void add(String keyName, int keyValue) {
    keys.put(keyName, keyValue);
  }

  public int getValue(String keyName) {
    if(keys.get(keyName) != null) {
      return keys.get(keyName);
    }

    return -1;
  }

  public void initializeAsEnemy(int id) {
    switch(id) {
      case 2:
        add("Left", Keys.LEFT);
        add("Right", Keys.RIGHT);
        break;
      case 3:
        add("Left", Keys.J);
        add("Right", Keys.L);
        break;
      case 4:
        add("Left", Keys.N4);
        add("Right", Keys.N6);
        break;
    }
  }

  public void initializeAsPlayer(int id) {
    switch(id) {
      case 1:
        add("Up", Keys.Z);
        add("Down", Keys.S);
        add("Left", Keys.Q);
        add("Right", Keys.D);
        add("Action", Keys.SPACE);
        break;
      case 2:
        add("Up", Keys.UP);
        add("Down", Keys.DOWN);
        add("Left", Keys.LEFT);
        add("Right", Keys.RIGHT);
        add("Action", Keys.CTRL);
        break;
      case 3:
        add("Up", Keys.I);
        add("Down", Keys.K);
        add("Left", Keys.J);
        add("Right", Keys.L);
        add("Action", Keys.N);
        break;
      case 4:
        add("Up", Keys.N8);
        add("Down", Keys.N5);
        add("Left", Keys.N4);
        add("Right", Keys.N6);
        add("Action", Keys.ENTER);
        break;
    }
  }
}