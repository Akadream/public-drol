/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.inputs;

import java.awt.event.*;

/**
 *
 * @author Akadream
 */
public class Keys {
  public static final int A = KeyEvent.VK_A;
  public static final int Z = KeyEvent.VK_Z;
  public static final int E = KeyEvent.VK_E;
  public static final int R = KeyEvent.VK_R;
  public static final int T = KeyEvent.VK_T;
  public static final int Y = KeyEvent.VK_Y;
  public static final int U = KeyEvent.VK_U;
  public static final int I = KeyEvent.VK_I;
  public static final int O = KeyEvent.VK_O;
  public static final int P = KeyEvent.VK_P;
  public static final int Q = KeyEvent.VK_Q;
  public static final int S = KeyEvent.VK_S;
  public static final int D = KeyEvent.VK_D;
  public static final int F = KeyEvent.VK_F;
  public static final int G = KeyEvent.VK_G;
  public static final int H = KeyEvent.VK_H;
  public static final int J = KeyEvent.VK_J;
  public static final int K = KeyEvent.VK_K;
  public static final int L = KeyEvent.VK_L;
  public static final int M = KeyEvent.VK_M;
  public static final int W = KeyEvent.VK_W;
  public static final int X = KeyEvent.VK_X;
  public static final int C = KeyEvent.VK_C;
  public static final int V = KeyEvent.VK_V;
  public static final int B = KeyEvent.VK_B;
  public static final int N = KeyEvent.VK_N;
  public static final int CTRL = KeyEvent.VK_CONTROL;
  public static final int SHIFT = KeyEvent.VK_SHIFT;
  public static final int ALT = KeyEvent.VK_ALT;
  public static final int ALT_GR = KeyEvent.VK_ALT_GRAPH;
  public static final int SPACE = KeyEvent.VK_SPACE;
  public static final int UP = KeyEvent.VK_UP;
  public static final int DOWN = KeyEvent.VK_DOWN;
  public static final int LEFT = KeyEvent.VK_LEFT;
  public static final int RIGHT = KeyEvent.VK_RIGHT;
  public static final int ENTER = KeyEvent.VK_ENTER;
  public static final int ESCAPE = KeyEvent.VK_ESCAPE;
  public static final int TAB = KeyEvent.VK_TAB;
  public static final int N0 = KeyEvent.VK_NUMPAD0;
  public static final int N1 = KeyEvent.VK_NUMPAD1;
  public static final int N2 = KeyEvent.VK_NUMPAD2;
  public static final int N3 = KeyEvent.VK_NUMPAD3;
  public static final int N4 = KeyEvent.VK_NUMPAD4;
  public static final int N5 = KeyEvent.VK_NUMPAD5;
  public static final int N6 = KeyEvent.VK_NUMPAD6;
  public static final int N7 = KeyEvent.VK_NUMPAD7;
  public static final int N8 = KeyEvent.VK_NUMPAD8;
  public static final int N9 = KeyEvent.VK_NUMPAD9;
}