/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.sound;

import drol.Game;
import java.io.IOException;
import javax.sound.sampled.*;

/**
 *
 * @author Akadream
 */
public class MakeSound {
    public static synchronized void playSound(final String url, boolean loop) {
    new Thread(() -> {
        try {
            Clip clip = AudioSystem.getClip();
            AudioInputStream inputStream = AudioSystem.getAudioInputStream(
                    Game.class.getResourceAsStream("assets/" + url));
            clip.open(inputStream);
            if(loop) {
                clip.loop(Clip.LOOP_CONTINUOUSLY);
            }
            
            clip.start();
        } catch (IOException | LineUnavailableException | UnsupportedAudioFileException e) {
            System.err.println(e.getMessage());
        }
    } // The wrapper thread is unnecessary, unless it blocks on the
    ).start();
  }
}
