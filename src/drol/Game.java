/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol;

import drol.entities.Bird;
import drol.entities.Directions;
import drol.entities.Enemy;
import drol.entities.Human;
import drol.entities.Player;
import drol.entities.PlayerNet;
import drol.entities.Scorpio;
import drol.entities.Snake;
import drol.entities.Tileset;
import drol.scenes.Window;
import drol.inputs.Keys;
import drol.maps.MapManager;
import drol.maps.Spawn;
import drol.net.GameClient;
import drol.net.GameServer;
import drol.net.PacketHuman;
import drol.net.PacketLogin;
import drol.net.PacketMove;
import drol.sound.MakeSound;
import drol.time.Timer;
import drol.ui.ImageManager;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Akadream
 */
public final class Game extends JPanel implements KeyListener, Runnable {
  // Loop configuration
  public boolean mainLoop = true;
  private final long gameDuration = 300;
  private int fps = 0;
  private long lastFpsTime = 0;
  private Thread thread;
  private Timer gameTimer;

  private final Window window;

  // Map and player
  private final MapManager map;
  private final Tileset tileset;

  private final Image titlescreen;

  private List<Player> players;
  private int playerCount = 0;
  private int previousEnemyTaken = -1;
  private int currentEnemyTaken = -1;
  static boolean coopWithEnemies;
  private int score;

  public boolean isApplet = false;

  // Timers
  private final Timer spawningTimer;

  // Game states
  static final int GAME_MENU = 0;
  static final int GAME = 1;
  static final int GAME_ONLINE = 11;
  static final int GAME_FINISH_LEVEL = 2;
  static final int GAME_OVER = 3;

  public static int gameState = GAME_MENU;
  protected int currentLevel = 1;

  // List of possible enemies spawn and humans spawn
  private final List<Spawn> spawns;
  private final List<Spawn> humanSpawns;

  // List of entities
  public Human currentHuman;
  private final List<Enemy> enemies;
  private int humansSaved = 0;

  // Netwrok
  public GameServer socketServer;
  public GameClient socketClient;

  /**
   * Game constructor
     * @param window The window wich contain the game
   */
  public Game(Window window) {
    this.window = window;
    this.window.addKeyListener(this);
    this.window.setContentPane(this);

    // Instances of objects
    spawns = new ArrayList<>();
    humanSpawns = new ArrayList<>();
    spawningTimer = new Timer();
    enemies = new ArrayList<>();
    players = new ArrayList<>();
    tileset = new Tileset("assets/Tileset.png");

    ImageManager im = new ImageManager("assets/TitleScreen.png");
    titlescreen = im.getSprite();

    // Map
    map = new MapManager();

    // Create spawns
    createSpawns();

    playerCount = 1;

    // Game reset
    gameInitialization();
  }

  /**
   * Function wich create players and add them to a list
   */
  public void createPlayers() {
    // Create players
    if(coopWithEnemies) {
      players.add(new Player(1, JOptionPane.showInputDialog(this, "Please enter a username"), map));
    }
    else {
      for(int i = 1; i <= playerCount; i++) {
        players.add(new Player(i, JOptionPane.showInputDialog(this, "Please enter a username"), map));
      }
    }

    // Make every player can use the key listening
    this.window.addPlayersListener(players);
  }

  /**
   * Adding an online player to the players list
   * @param player Player have to be added
   */
  public void addPlayerNet(PlayerNet player) {
    players.add(player);
  }

  /**
   * Get the map where are playing the players
   * @return 
   */
  public MapManager getMap() {
    return this.map;
  }

  /**
   * Use to set the current state of the game to synchronize every players
   * @param timer Current time of the game
   */
  public void setParameters(long timer) {
    gameTimer = new Timer(timer);
  }

  /**
   * Move a player on the map
   * @param username Username of the player
   * @param x New x position of the player
   * @param y New y position of the player
   * @param direction New direction of the player
   */
  public void movePlayer(String username, float x, float y, Directions direction) {
    for(Player player : players) {
      if(player.getUsername().equalsIgnoreCase(username)) {
        player.setX(x);
        player.setY(y);
        player.setDirection(direction);
      }
    }
  }

  /**
   * Clear every list of entities
   */
  public void clear() {
    this.players.clear();
    this.enemies.clear();
  }

  /**
   * Remove a player by is username
   * @param username [username of the player]
   */
  public void removePlayer(String username) {
    for(int i = 0; i < players.size(); i++) {
      if(players.get(i).getUsername().equalsIgnoreCase(username)) {
        players.remove(i);
      }
    }
  }

  /**
   * Create a new online player
   */
  public void createPlayersNet() {
    for(int i = 1; i <= playerCount; i++) {
      String username = JOptionPane.showInputDialog(this, "Please enter a username");
      PlayerNet player = new PlayerNet(i, username, map, null, -1);
      PacketLogin packet = new PacketLogin(username, (float)(Settings.START_X * 32 - 1), (float)(Settings.START_Y * 32 - 1));
      players.add(player);

      if(socketServer != null) {
        socketServer.addConnection((PlayerNet) player, packet);
      }

      packet.writeData(socketClient);
    }

    // Make every player can use the key listening
    window.addPlayersListener(players);
  }

  /**
   * Get the current list of players
   * @return 
   */
  public List<Player> getPlayers() {
    return this.players;
  }

  /**
   * Update the players list
   * @param players 
   */
  public void setPlayers(List<Player> players) {
    this.players = players;
  }

  /**
   * This function initialize game properties / attributes
   */
  public void gameInitialization() {
    score = 0;
    gameTimer = new Timer();
  }

  /**
   * Setup the game server
   */
  public void setupServer() {
    socketServer = new GameServer(this);
    socketServer.start();
  }

  /**
   * Setup the client server
   */
  public void setupClient() {
    socketClient = new GameClient(this, "localhost");
    socketClient.start();
  }

  /**
   * Update human on the map
   * @param x New x of the human
   * @param y New y of the human
   */
  public void setHuman(float x, float y) {
    System.out.println("Set human");
    currentHuman.setX(x);
    currentHuman.setY(y);
  }

  /**
   * Create spawn points
   */
  private void createSpawns() {
    for(int i = 0; i < 2; i++) {
      for(int j = 0; j < 5; j++) {
        int posX, posY;
        boolean humanSpawn = j >= 3 ? true : false;

        posX = i * 19 * 32;
        posY = (j * 3 + 2) * 32;

        Directions spawnDirection = i == 0 ? Directions.RIGHT : Directions.LEFT;

        spawns.add(new Spawn(posX, posY, spawnDirection, humanSpawn));
      }
    }

    // Set human spawns
    for(int i = 0; i < spawns.size(); i++) {
      if(spawns.get(i).canHumanSpawn()) {
        humanSpawns.add(spawns.get(i));
      }
    }
  }

  /**
   * Create a human and define his position on the map
   */
  private void spawnHuman() {
    if(gameState == GAME || (gameState == GAME_ONLINE && socketServer != null)) {
      System.out.println("Test adding human");
      int randomPosition = (int)(Math.random() * (humanSpawns.size() - 1) + 1);
      int tileX = humanSpawns.get(randomPosition).getX();
      int tileY = humanSpawns.get(randomPosition).getY();
      Directions humanDirection = humanSpawns.get(randomPosition).getDirection();

      currentHuman =  new Human(tileX, tileY, 32, 32, 0, 0, 32, 32, "assets/Robot.png", humanDirection);
      if(socketServer != null) {
        PacketHuman packet = new PacketHuman(currentHuman.getX(), currentHuman.getY());
        packet.writeData(socketClient);
      }
    }
  }

  /**
   * Function wich create an enemy and define his position on the map
   */
  private void spawnEnemy() {
    // Only the player wich is the server can make spawn enemies
    if(gameState == GAME || (gameState == GAME_ONLINE && socketServer != null)) {
      // Get a random position to the enemy
      int randomPosition = (int)(Math.random() * (spawns.size() - 1) + 1);
      // Get enemy type
      int randomEnemyType = (int)(Math.random() * (4 - 1) + 1);

      int tileX = spawns.get(randomPosition).getX();
      int tileY = spawns.get(randomPosition).getY();
      Directions enemyDirection = spawns.get(randomPosition).getDirection();

      switch(randomEnemyType) {
        case 1:
          // Snake
          enemies.add(new Snake(tileX, tileY, 32, 32, 0, 0, 32, 32, "assets/Robot.png", enemyDirection));
          break;
        case 2:
          // Scorpio
          enemies.add(new Scorpio(tileX, tileY, 32, 32, 0, 0, 32, 32, "assets/Robot.png", enemyDirection));
          break;
        case 3:
          // Bird
          enemies.add(new Bird(tileX, tileY, 32, 32, 0, 0, 32, 32, "assets/Robot.png", enemyDirection));
          break;
      }
    }
  }

  /**
   * Player use the next monster of the list
   */
  private void useNextMonster() {
    if(enemies.size() > 0) {
      if(previousEnemyTaken == -1) {
        currentEnemyTaken = 0;
      }
      else if(enemies.size() > previousEnemyTaken + 1) {
        currentEnemyTaken = previousEnemyTaken + 1;
      }
      else if(previousEnemyTaken + 1 == enemies.size() && enemies.size() >= 1) {
        currentEnemyTaken = 0;
      }
    }
    else {
      currentEnemyTaken = -1;
    }

    previousEnemyTaken = currentEnemyTaken;

    if(currentEnemyTaken >= 0) {
      enemies.get(currentEnemyTaken).setUserId(2);
      window.addEnemyListener(enemies.get(currentEnemyTaken));
    }
  }

  /**
   * Check pressed inputs
   * @param e KeyEvent to detect keys informations
   */
  @Override
  public void keyPressed(KeyEvent e) {
    switch(gameState) {
      case GAME_MENU:
        if(e.getKeyCode() == Keys.N1) {
          // Solo
          this.playerCount = 1;
          createPlayers();
          gameState = GAME;
          spawnHuman();
        }
        else if(e.getKeyCode() == Keys.N2) {
          // Multi
          this.playerCount = Integer.parseInt(JOptionPane.showInputDialog(this, "Number of players"));
          if(this.playerCount < 2) this.playerCount = 2;
          if(this.playerCount > 4) this.playerCount = 4;
          setCoopWithEnemies(false);
          createPlayers();
          gameState = GAME;
          spawnHuman();
        }
        else if(e.getKeyCode() == Keys.N3) {
          // Réseau
          this.playerCount = 1;
          setCoopWithEnemies(false);
          if(socketServer == null && JOptionPane.showConfirmDialog(this, "Do you want to run the server") == 0) {
            setupServer();
          }
          if(socketClient == null) setupClient();
          createPlayersNet();
          gameState = GAME_ONLINE;
          spawnHuman();
        }
        else if(e.getKeyCode() == Keys.N4) {
          // Quitter
          stop();
        }
        break;
      case GAME:
        if(e.getKeyCode() == Keys.ESCAPE) {
          gameState = GAME_MENU;
          this.clear();
        }
        if(currentEnemyTaken != -1) {
          if(e.getKeyCode() == Keys.SHIFT) {
            enemies.get(previousEnemyTaken).setUserId(0);
            currentEnemyTaken = -1;
          }
        }
        break;
      case GAME_ONLINE:
        if(e.getKeyCode() == Keys.ESCAPE) {
          gameState = GAME_MENU;
          this.clear();
        }
        break;
    }
  }

  /**
   * Check released inputs
   * @param e KeyEvent to detect keys informations
   */
  @Override
  public void keyReleased(KeyEvent e) { }

  /**
   * KeyTyped. This function is needed but we dosen't need to use it
   * @param e
   */
  @Override
  public void keyTyped(KeyEvent e) { }

  /**
   * Check interactions between player and human
   */
  private void updateHumans(double delta) {
    // If we release 3 humans
    if(humansSaved >= Settings.humansToSave) {
      // Convert remaining lives to score when the game is ending
      stop();
    }

    // If there is a human on the map
    if(currentHuman != null) {
      currentHuman.update(delta);
      if(socketServer != null) {
        PacketHuman packet = new PacketHuman(currentHuman.getX(), currentHuman.getY());
        packet.writeData(socketClient);
      }
      for(Player player : players) {
        if(player.isColliding(currentHuman) && !currentHuman.isHurted()) {
          score += 56;
          MakeSound.playSound("HumanCollision.wav", false);
          humansSaved++;
          currentHuman.setHurted(true);
          spawnHuman();

          if(player.isInvulnerable()) {
            player.setInvulnerable(false);
          }
        }
      }
    }
  }

  /**
   * Check interactions between player and enemies
   */
  private void updateEnemies(double delta) {
    // If there is at least one enemy one the map
    if(enemies.size() > 0) {
      for(int i = 0; i < enemies.size(); i++) {
        enemies.get(i).update(delta);

        for(Player player : players) {
          // If there is a collision between the player and one enemy
          if(player.isColliding(enemies.get(i)) && !player.isInvulnerable()) {
            MakeSound.playSound("MonsterCollision.wav", false);
            score += 35;
            enemies.remove(i);
            player.die();
            return;
          }

          for(int j = 0; j < player.getLasers().size(); j++) {
            if(player.getLasers().get(j).isColliding(enemies.get(i))) {
              player.getLasers().remove(j);
              if(enemies.get(i).getUserId() == 2) {
                currentEnemyTaken = -1;
              }
              enemies.remove(i);
              score += 35;
              return;
            }
          }
        }
      }
    }
    else {
      // Spawn automatically an enemy if no one is on the map
      spawnEnemy();
    }

    // Spawn an enemy ever 5 seconds if the is less than 10 enemies on the map
    if(spawningTimer.getElapasedTime() >= 5000) {
      if(enemies.size() < 10) {
        spawnEnemy();
      }

      // Reset the timer
      spawningTimer.reset();
    }
  }

  /**
   * Update function wich is called every frame. This function run the game logic
   * @param delta The time between two frames
   */
  protected void update(double delta) {
    switch(gameState) {
      case GAME_MENU:
        break;
      case GAME:
        for(Player player : players) {
          if(!player.isDead()) {
            player.update(delta);
          }
        }

        updateHumans(delta);
        updateEnemies(delta);

        if(currentEnemyTaken == -1 && playerCount == 2 && coopWithEnemies) {
          useNextMonster();
        }

        if(everyPlayersDead()) {
          stop();
        }
        break;
      case GAME_ONLINE:
        for(Player player : players) {
          if(!player.isDead()) {
            player.update(delta);
            if(player.isMoving()) {
              PacketMove packet = new PacketMove(player.getUsername(), player.getX(), player.getY(), player.getDirection());
              packet.writeData(socketClient);
            }
          }
        }

        updateHumans(delta);
        updateEnemies(delta);

        if(currentEnemyTaken == -1 && playerCount == 2 && coopWithEnemies) {
          useNextMonster();
        }
        break;
    }
  }

  /**
   * Drawing function wich is called every frame. This function draw all entities on the screen
   */
  protected void draw() {
    repaint();
  }

  /**
   * Function called by the draw function. Draw everything of the game
   * @param g Graphics compononent to draw images, rectangles and texts
   */
  @Override
  public void paintComponent(Graphics g) {
    Font arial = new Font("Arial", Font.BOLD, 14);
    Color white = new Color(255, 255, 255);
    g.setFont(arial);
    g.setColor(white);

    switch(gameState) {
      case GAME_MENU:
        drawTitleScreen(g);
        break;
      case GAME:
        drawMap(g);
        drawEntities(g);
        drawUI(g);
        break;
      case GAME_ONLINE:
        drawMap(g);
        drawOnlineEntities(g);
        drawUI(g);
        break;
    }
  }

  /**
   * Function to draw the map
   * @param g Graphics used to draw on the screen
   */
  public void drawMap(Graphics g) {
    for(int x = 0; x < MapManager.MAP_WIDTH; x++) {
      for(int y = 0; y < MapManager.MAP_HEIGHT; y++) {
        // Draw map
        tileset.drawTile(g, this, map.getTile(x, y));
      }
    }
  }

  /**
   * Function to draw entities
   * @param g Graphics used to draw on the screen
   */
  public void drawEntities(Graphics g) {
    // Draw player, entities
    for(Player player : players) {
      if(!player.isDead()) {
        player.draw(g, this);
      }
    }

    // Draw human
    if(currentHuman != null && !currentHuman.isHurted()) {
      currentHuman.draw(g, this);
    }

    for(Enemy enemy : enemies) {
      enemy.draw(g, this);
    }
  }

  /**
   * Draw online entities on the map
   * @param g Graphics to draw
   */
  public void drawOnlineEntities(Graphics g) {
    // Draw player, entities
    for(Player player : players) {
      if(!player.isDead()) {
        player.draw(g, this);
      }
    }

    // Draw human
    if(currentHuman != null && !currentHuman.isHurted()) {
      currentHuman.draw(g, this);
    }

    for(Enemy enemy : enemies) {
      enemy.draw(g, this);
    }
  }

  /**
   * Function to draw UI
   * @param g Graphics used to draw on the screen
   */
  public void drawUI(Graphics g) {
    // Color of texts
    g.setColor(new Color(255, 255, 255));

    // Draw HUD
    int totalLifes = 0;
    for(Player player : players) {
      totalLifes += player.getLife();
    }

    g.drawString("Lifes : " + totalLifes, 10, 20);
    g.drawString("Score : " + score, 10, 35);
    g.drawString("Humans saved : " + humansSaved, 10, 50);
    g.drawString("Time remaining : " + (gameDuration - gameTimer.getElapasedTime() / 1000), 10, 65);
  }

  /**
   * Draw the titlescreen on the screen
   * @param g Graphics to draw
   */
  public void drawTitleScreen(Graphics g) {
    g.drawImage(titlescreen,
                0,
                0,
                608,
                512,
                this);
  }

  /**
   * Starting function wich make this game running thanks to Thread system
   */
  public synchronized void start() {
    mainLoop = true;

    thread = new Thread(this, "Main");
    thread.start();
  }

  /**
   * Function called during the end of the game when objectives are reached
   */
  public synchronized void stop() {
    System.out.println("Game ended.");
    mainLoop = false;
    try {
      thread.join();
      System.exit(0);
    }
    catch(InterruptedException e) {
    }
  }

  /**
   * Check if every players of the game are dead
   * @return True if every players are dead
   */
  private boolean everyPlayersDead() {
    for(Player player : players) {
      if(!player.isDead()) {
        return false;
      }
    }

    return true;
  }

  /**
   * Check if the game timer has ended
   * @return True if the timer is 0
   */
  private boolean timerEnded() {
    if(gameTimer.getElapasedTime() / 1000 >= gameDuration) {
      return true;
    }

    return false;
  }

  /**
   * Set the game mode cooperation
   * @param b boolean
   */
  public void setCoopWithEnemies(boolean b) {
    this.coopWithEnemies = b;
  }

  /**
   * The game loop mechanics
   */
  public void gameLoop() {
    // Game loop logic
    long lastLoopTime = System.nanoTime();
    final int TARGET_FPS = 60;
    final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;

    while(mainLoop) {
      long now = System.nanoTime();
      long updateLength = now - lastLoopTime;
      lastLoopTime = now;
      double delta = updateLength / ((double)OPTIMAL_TIME);

      lastFpsTime += updateLength;
      fps++;

      if(lastFpsTime >= 1000000000) {
        this.window.setTitle("Drol - Guillaume and Oceane - FPS : " + fps);
        lastFpsTime = 0;
        fps = 0;
      }

      // Game update
      // Do the game logic
      // Update
      update(delta);
      // Rendering
      draw();

      long time = ((lastLoopTime - System.nanoTime() + OPTIMAL_TIME) / 1000000);
      if(time < 0) time = 0;

      try {
        Thread.sleep(time);
      }
      catch(InterruptedException e) {
        // Do nothing when catching exception
      }
    }
  }

  /**
   * Run function called when a thread is started. This function call the game loop
   */
  @Override
  public void run() {
    MakeSound.playSound("Drol.wav", true);
    gameLoop();
  }
}