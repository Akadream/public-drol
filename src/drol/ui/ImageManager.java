/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.ui;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

/**
 *
 * @author Akadream
 */
public class ImageManager {
    private BufferedImage sprite;

  public ImageManager(String filename) {
    try {
      sprite = ImageIO.read(new File(filename));
    }
    catch(IOException e) {
      System.out.println("Error during image loading...");
    }
  }

  public void setSprite(String filename) {
    try {
      sprite = ImageIO.read(new File(filename));
    }
    catch(IOException e) {
      System.out.println("Error during image loading.");
    }
  }

  public int getWidth() {
    return sprite.getWidth();
  }

  public int getHeight() {
    return sprite.getHeight();
  }

  public Image getSprite() {
    return (Image)sprite;
  }
}
