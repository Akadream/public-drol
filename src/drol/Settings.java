/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol;

/**
 *
 * @author Akadream
 */
public class Settings {
  public static final int humansToSave = 99;
  public static final int windowWidth = 800;
  public static final int windowHeight = 600;
  public static final float pixelScaling = 1.8f;
  public static final int tileSize = 32;
  public static final int WORLD_WIDTH = (19 * 32);
  public static final int WORLD_HEIGHT = (16 * 32);
  // Starting position
  public static final int START_X = 9;
  public static final int START_Y = 2;
}
