/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.entities;

import drol.collisions.Collider;
import java.awt.*;
import javax.swing.JPanel;

/**
 *
 * @author Akadream
 */
public abstract class Entity {
  protected float x;
  protected float y;
  private int width;
  private int height;

  private Collider collider;
  protected Image sprite;

  public Entity(float x, float y, int width, int height, int colliderX, int colliderY, int colliderWidth, int colliderHeight) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.collider = new Collider(colliderX, colliderY, colliderWidth, colliderHeight);
  }

  public float getX() {
    return x;
  }

  public float getY() {
    return y;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  public int getRight() {
    return (int)(x + width);
  }

  public int getBottom() {
    return (int)(y + height);
  }

  public int getMiddleX() {
    return (int)(x + width / 2);
  }

  public int getMiddleY() {
    return (int)(y + height / 2);
  }

  public void setX(float x) {
    this.x = x;
  }

  public float getAbsoluteLeft() {
    return this.x + this.getCollider().getX();
  }

  public float getAbsoluteRight() {
    return this.x + this.getCollider().getX() + this.getCollider().getWidth();
  }

  public float getAbsoluteTop() {
    return this.y + this.getCollider().getY();
  }

  public float getAbsoluteBottom() {
    return this.y + this.getCollider().getY() + this.getCollider().getHeight();
  }

  public void setY(float y) {
    this.y = y;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public boolean isColliding(Entity entity) {
    return !(
      entity.getAbsoluteLeft() > this.getAbsoluteRight() ||
      entity.getAbsoluteRight() < this.getAbsoluteLeft() ||
      entity.getAbsoluteTop() > this.getAbsoluteBottom() ||
      entity.getAbsoluteBottom() < this.getAbsoluteTop()
    );
  }

  public Collider getCollider() {
    return this.collider;
  }

  public Image getSprite() {
    return this.sprite;
  }

  // These functions can't be called because every enemies behavior are differents
  public abstract void update(double delta);
  public abstract void draw(Graphics g, JPanel p);
}