/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.entities;

import drol.Settings;
import drol.inputs.KeyManager;
import drol.maps.MapManager;
import drol.sound.MakeSound;
import drol.time.Timer;
import drol.ui.ImageManager;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author Akadream
 */
public class Player extends Entity implements KeyListener {
  // Player attributes
  private final int playerId;
  private int life;
  private final String username;
  private Directions direction;
  private boolean onGround;
  private boolean invulnerable;

  private final List<Laser> lasers;

  // Transitions
  private boolean transitioning = false;
  private float transitionX;
  private float transitionY;
  private boolean transitionToBottom;

  private final MapManager map;

  // Directions
  private boolean goLeft;
  private boolean goRight;
  private boolean goUp;
  private boolean goDown;
  private boolean previouslyKeyDown;
  private boolean moving = false;

  private final KeyManager keys;

  private final Timer fireTimer;

  private final float speed = 1f;

  public Player(int playerId, String username, MapManager map) {
    super(Settings.START_X * 32 - 1, Settings.START_Y * 32 - 1, 32, 32, 7, 9, 18, 23);

    ImageManager im = new ImageManager("assets/Robot.png");
    lasers = new ArrayList<>();

    // Keys listening initialization
    keys = new KeyManager();
    keys.initializeAsPlayer(playerId);

    this.sprite = im.getSprite();
    this.direction = Directions.LEFT;
    this.playerId = playerId;
    this.map = map;
    this.username = username;

    this.fireTimer = new Timer();

    this.life = 3;
    this.onGround = true;
  }

  public float getSpeed() {
    return this.speed;
  }

  public int getLife() {
    return this.life;
  }

  public boolean isOnGround() {
    return onGround;
  }

  public void setOnGround(boolean onGround) {
    this.onGround = onGround;
  }

  public void setDirection(Directions direction) {
    this.direction = direction;
  }

  public Directions getDirection() {
    return this.direction;
  }

  public void setInvulnerable(boolean invulnerable) {
    this.invulnerable = invulnerable;
  }

  public boolean isInvulnerable() {
    return this.invulnerable;
  }

  public void die() {
    if(this.life > 0)
      this.life--;
      this.setY(63);
      this.setInvulnerable(true);
      this.transitioning = false;
      this.onGround = true;
  }

  public boolean isDead() {
    return this.life <= 0;
  }

  public List<Laser> getLasers() {
    return this.lasers;
  }

  /**
   * Function wich manage everything to do in terms of inputs pressed
   */
  private void handleInputs(double delta) {
    if(goLeft) {
      this.setDirection(Directions.LEFT);
      if(this.getPositionToLeft() > 0) {
        if(!this.isColliding(map.getTile((int)this.getPositionToLeft() / 32, (int)this.getAbsoluteTop() / 32)) &&
           !this.isColliding(map.getTile((int)this.getPositionToLeft() / 32, (int)this.getAbsoluteBottom() / 32)) &&
           !map.getTile((int)this.getPositionToLeft() / 32, (int)this.getAbsoluteTop() / 32).canCollide() &&
           !map.getTile((int)this.getPositionToLeft() / 32, (int)this.getAbsoluteBottom() / 32).canCollide()) {
             this.setX((float)(this.getX() - this.getSpeed() * delta));
        }
      }
    }
    else if(goRight) {
      this.setDirection(Directions.RIGHT);
      if(this.getPositionToRight() < 607) {
        if(!this.isColliding(map.getTile((int)this.getPositionToRight() / 32, (int)this.getAbsoluteTop() / 32)) &&
           !this.isColliding(map.getTile((int)this.getPositionToRight() / 32, (int)this.getAbsoluteBottom() / 32)) &&
           !map.getTile((int)this.getPositionToRight() / 32, (int)this.getAbsoluteTop() / 32).canCollide() &&
           !map.getTile((int)this.getPositionToRight() / 32, (int)this.getAbsoluteBottom() / 32).canCollide()) {
             this.setX((float)(this.getX() + this.getSpeed() * delta));
        }
      }
    }

    if(goUp) {
      if(this.getPositionToTop() > 0 && !previouslyKeyDown) {
        if(this.isOnGround()) {
          if(!this.isColliding(map.getTile((int)this.getAbsoluteLeft() / 32, (int)this.getPositionToTop() / 32)) &&
             !this.isColliding(map.getTile((int)this.getAbsoluteRight() / 32, (int)this.getPositionToTop() / 32)) &&
             !map.getTile((int)this.getAbsoluteLeft() / 32, (int)this.getPositionToTop() / 32).canCollide() &&
             !map.getTile((int)this.getAbsoluteRight() / 32, (int)this.getPositionToTop() / 32).canCollide()) {
               transitioning = true;
               transitionX = this.getX();
               transitionY = this.getY() - 32;
               this.setOnGround(false);
          }
        }
        else {
          // Isn't on ground
          if(map.getTileID((int)this.getAbsoluteLeft() / 32, (int)this.getPositionToTop() / 32) == 5 &&
             map.getTileID((int)this.getAbsoluteRight() / 32, (int)this.getPositionToTop() / 32) == 5) {
               transitioning = true;
               transitionX = this.getX();
               transitionY = this.getY() - 64;
               this.setOnGround(true);
          }
        }
      }
    }
    else if(goDown) {
      if(this.getPositionToBottom() < 639 && !previouslyKeyDown) {
        if(!this.isOnGround()) {
          if(!this.isColliding(map.getTile((int)this.getAbsoluteLeft() / 32, (int)this.getPositionToBottom() / 32)) &&
             !this.isColliding(map.getTile((int)this.getAbsoluteRight() / 32, (int)this.getPositionToBottom() / 32)) &&
             !map.getTile((int)this.getAbsoluteLeft() / 32, (int)this.getPositionToBottom() / 32).canCollide() &&
             !map.getTile((int)this.getAbsoluteRight() / 32, (int)this.getPositionToBottom() / 32).canCollide()) {
               transitioning = true;
               transitionX = this.getX();
               transitionY = this.getY() + 32;
               this.setOnGround(true);
          }
        }
        else {
          // On ground
          if(map.getTileID((int)this.getAbsoluteLeft() / 32, (int)this.getPositionToBottom() / 32) == 5 &&
             map.getTileID((int)this.getAbsoluteRight() / 32, (int)this.getPositionToBottom() / 32) == 5) {
               transitioning = true;
               transitionX = this.getX();
               transitionY = this.getY() + 64;
               this.setOnGround(false);
          }
        }
      }
    }

    // We move, let's send the packet !
    moving = goUp || goDown || goLeft || goRight;

    previouslyKeyDown = goUp || goDown || goLeft || goRight;
  }

  public boolean isMoving() {
    return this.moving;
  }

  @Override
  public void update(double delta) {
    // If the player is making a transition from one floor to another one
    if(transitioning) {
      if(this.getY() < transitionY) {
        this.setY((float)(this.getY() + this.getSpeed()));
      }
      else if(this.getY() > transitionY) {
        this.setY((float)(this.getY() - this.getSpeed()));
      }
      else {
        transitioning = false;
      }
    }
    else {
      // We can use the input system only if the player isn't currently interacting with an entity (or himself)
      handleInputs(delta);
    }

    updateLasers(delta);
  }

  /**
   * Firing a laser when we press space key
   */
  public void fire() {
    if(fireTimer.getElapasedTime() >= 500 && !transitioning) {
      MakeSound.playSound("Laser.wav", false);
      lasers.add(new Laser(this.getX(), this.getMiddleY(), this.getDirection()));
      fireTimer.reset();
    }
  }

  /**
   * Update every lasers wich are on the map
   */
  private void updateLasers(double delta) {
    for(int i = 0; i < lasers.size(); i++) {
      if(lasers.get(i).getRight() <= 0 || lasers.get(i).getX() >= 614) {
        lasers.remove(i);
        return;
      }

      lasers.get(i).update(delta);
    }
  }

  /**
   * Check pressed inputs
   * @param e KeyEvent to detect keys informations
   */
  @Override
  public void keyPressed(KeyEvent e) {
    if(e.getKeyCode() == keys.getValue("Up")) {
      // Up
      goUp = true;
      if(this.isInvulnerable()) {
        this.setInvulnerable(false);
      }
    }
    else if(e.getKeyCode() == keys.getValue("Down")) {
      // Down
      goDown = true;
      if(this.isInvulnerable()) {
        this.setInvulnerable(false);
      }
    }

    if(e.getKeyCode() == keys.getValue("Left")) {
      goLeft = true;
      if(this.isInvulnerable()) {
        this.setInvulnerable(false);
      }
    }
    else if(e.getKeyCode() == keys.getValue("Right")) {
      goRight = true;
      if(this.isInvulnerable()) {
        this.setInvulnerable(false);
      }
    }

    if(e.getKeyCode() == keys.getValue("Action")) {
      fire();
    }
  }

  /**
   * Check released inputs
   * @param e KeyEvent to detect keys informations
   */
  @Override
  public void keyReleased(KeyEvent e) {
    if(e.getKeyCode() == keys.getValue("Up")) {
      // Up
      goUp = false;
    }
    if(e.getKeyCode() == keys.getValue("Down")) {
      // Down
      goDown = false;
    }

    if(e.getKeyCode() == keys.getValue("Left")) {
      goLeft = false;
    }

    if(e.getKeyCode() == keys.getValue("Right")) {
      goRight = false;
    }
  }

  /**
   * KeyTyped. This function is needed but we dosen't need to use it
   * @param e [description]
   */
  @Override
  public void keyTyped(KeyEvent e) {
  }

  public float getPositionToTop() {
    return this.getY() + this.getCollider().getY() - 32;
  }

  public float getPositionToBottom() {
    return this.getY() + this.getCollider().getY() + this.getCollider().getHeight() + 32;
  }

  public float getPositionToLeft() {
    return this.getX() + this.getCollider().getX() - this.speed;
  }

  public float getPositionToRight() {
    return this.getX() + this.getCollider().getX() + this.getCollider().getWidth() + this.speed;
  }

  @Override
  public void draw(Graphics g, JPanel p) {
    // Default position of the player sprite
    int srcX = 0;
    int srcY = 0;

    // Get the sprite corresponding with the player orientation
    switch(this.direction) {
      case LEFT:
        srcX = 32;
        break;
      case RIGHT:
        srcX = 0;
        break;
    }

    // Draw player sprite
    g.drawImage(this.getSprite(),
                (int)this.getX(),
                (int)this.getY(),
                (int)this.getX() + this.getWidth(),
                (int)this.getY() + this.getHeight(),
                srcX,
                srcY,
                srcX + 32,
                srcY + 32,
                p);

    // Draw player indicator
    g.setColor(new Color(255, 255, 255));
    int[] pointsX = new int[3];
    int[] pointsY = new int[3];
    pointsX[0] = (int)this.getX() + 10;
    pointsX[1] = (int)this.getRight() - 11;
    pointsX[2] = (int)this.getMiddleX();

    pointsY[0] = (int)this.getY() - 8;
    pointsY[1] = (int)this.getY() - 8;
    pointsY[2] = (int)this.getY() - 3;
    g.fillPolygon(pointsX, pointsY, 3);

    int stringWidth = g.getFontMetrics().stringWidth(this.username);
    g.drawString(this.username, (int)this.getMiddleX() - (int)(stringWidth / 2) + 1, (int)this.getY() - 10);

    lasers.forEach((laser) -> {
        laser.draw(g, p);
      });
  }

  public String getUsername() {
    return username;
  }
}
