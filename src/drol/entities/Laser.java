/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.entities;

import java.awt.*;
import javax.swing.JPanel;


/**
 *
 * @author Akadream
 */
public class Laser extends Entity {
    static final int LASER_WIDTH = 12;
  static final int LASER_HEIGHT = 12;

  private final float speed = 1.3f;
  private final Directions direction;

  public Laser(float x, float y, Directions direction) {
    super(x, y, LASER_WIDTH, LASER_HEIGHT, 0, 0, LASER_WIDTH, LASER_HEIGHT);

    this.direction = direction;
  }

    /**
     *
     * @param delta
     */
    @Override
  public void update(double delta) {
    switch(direction) {
      case LEFT:
        setX((float)(getX() - speed * delta));
        break;
      case RIGHT:
        setX((float)(getX() + speed * delta));
        break;
    }
  }

    /**
     *
     * @param g
     * @param p
     */
    @Override
  public void draw(Graphics g, JPanel p) {
    g.setColor(new Color(255, 0, 255));
    g.fillRect((int)(this.getX()),
               (int)(this.getY()),
               this.getWidth(),
               this.getHeight());
  }
}
