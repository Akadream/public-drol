/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.entities;

import drol.ui.ImageManager;
import java.awt.*;
import javax.swing.JPanel;

/**
 *
 * @author Akadream
 */
public final class Tileset {
  private Image sprite;

  public Tileset(String filename) {
    setSprite(filename);
  }

  public Image getSprite() {
    return sprite;
  }

  public void setSprite(String filename) {
    ImageManager im = new ImageManager(filename);
    sprite = im.getSprite();
  }

  public void drawTile(Graphics g, JPanel p, Tile tile) {
    g.drawImage(sprite,
                (int)(tile.getX()),
                (int)(tile.getY()),
                tile.getRight(),
                tile.getBottom(),
                (int)(tile.getId() % 8) * 32,
                (int)(tile.getId() / 8) * 32,
                (int)(tile.getId() % 8) * 32 + 32,
                (int)(tile.getId() / 8) * 32 + 32,
                p);

    // Draw collider box
    if(tile.getCollider() != null) {
      g.drawRect((int)(tile.getX() + tile.getCollider().getX()),
                 (int)(tile.getY() + tile.getCollider().getY()),
                 tile.getCollider().getWidth(),
                 tile.getCollider().getHeight());
    }
  }
}
