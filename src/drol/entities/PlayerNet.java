/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.entities;

import drol.maps.MapManager;
import java.net.InetAddress;

/**
 *
 * @author Akadream
 */
public class PlayerNet extends Player {
  public InetAddress ipAddress;
  public int port;

  public PlayerNet(int playerId, String username, MapManager map, InetAddress ipAddress, int port) {
    super(playerId, username, map);

    this.ipAddress = ipAddress;
    this.port = port;
  }

  public PlayerNet(int playerId, String username, float x, float y, MapManager map, InetAddress ipAddress, int port) {
    super(playerId, username, map);

    this.x = x;
    this.y = y;

    this.ipAddress = ipAddress;
    this.port = port;
  }
}