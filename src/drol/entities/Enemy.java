/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.entities;

import drol.inputs.KeyManager;
import drol.ui.ImageManager;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JPanel;

/**
 *
 * @author Akadream
 */
public abstract class Enemy extends Entity implements KeyListener {
  protected Directions direction;
  protected float speed;
  protected int control;
  protected KeyManager keys;
  protected int userId;

  public Enemy(float x, float y, int width, int height, int colliderX, int colliderY, int colliderWidth, int colliderHeight, String filename, Directions direction) {
    super(x, y, width, height, colliderX, colliderY, colliderWidth, colliderHeight);

    ImageManager im = new ImageManager(filename);
    this.sprite = im.getSprite();
    this.direction = direction;
    this.speed = 1.2f;
    this.userId = 0;

    keys = new KeyManager();
    keys.initializeAsEnemy(userId);
  }

  public void setUserId(int id) {
    this.userId = id;
    keys = new KeyManager();
    keys.initializeAsEnemy(this.userId);
  }

  public int getUserId() {
    return this.userId;
  }

  /**
   * Check pressed inputs
   * @param e KeyEvent to detect keys informations
   */
  @Override
  public void keyPressed(KeyEvent e) {
    if(userId == 2) {
      if(e.getKeyCode() == keys.getValue("Left")) {
        this.direction = Directions.LEFT;
      }
      else if(e.getKeyCode() == keys.getValue("Right")) {
        this.direction = Directions.RIGHT;
      }
    }
  }

  /**
   * Check released inputs
   * @param e KeyEvent to detect keys informations
   */
  @Override
  public void keyReleased(KeyEvent e) { }

  /**
   * KeyTyped. This function is needed but we dosen't need to use it
   * @param e
   */
  @Override
  public void keyTyped(KeyEvent e) { }

  @Override
  public void update(double delta) {
    switch(direction) {
        case LEFT:
          if(getRight() < 0) {
            setX(646);
          }

          setX((float)(getX() - speed * delta));
          break;
        case RIGHT:
          if(getX() > 614) {
            setX(-32);
          }

          setX((float)(getX() + speed * delta));
          break;
      }
  }

  @Override
  public void draw(Graphics g, JPanel p) {
    if(userId == 2) {
      // Draw player indicator
      g.setColor(new Color(255, 255, 255));
      int[] pointsX = new int[3];
      int[] pointsY = new int[3];
      pointsX[0] = (int)this.getX() + 10;
      pointsX[1] = (int)this.getRight() - 11;
      pointsX[2] = (int)this.getMiddleX();

      pointsY[0] = (int)this.getY() - 8;
      pointsY[1] = (int)this.getY() - 8;
      pointsY[2] = (int)this.getY() - 3;
      g.fillPolygon(pointsX, pointsY, 3);

      int stringWidth = g.getFontMetrics().stringWidth("J" + this.userId);
      g.drawString("J" + this.userId, (int)this.getMiddleX() - (int)(stringWidth / 2) + 1, (int)this.getY() - 10);
    }
  }
}