/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.entities;

import java.awt.*;
import javax.swing.JPanel;

/**
 *
 * @author Akadream
 */
public class Scorpio extends Enemy {
  private final float maxY;
  private final float minY;
  private final float jumpingSpeed = 0.7f;
  private boolean jumping;

  public Scorpio(float x, float y, int width, int height, int colliderX, int colliderY, int colliderWidth, int colliderHeight, String filename, Directions direction) {
    super(x, y, width, height, colliderX, colliderY, colliderWidth, colliderHeight, filename, direction);

    this.speed = 1.3f;

    maxY = y - 32;
    minY = y;
    jumping = false;
  }

  @Override
  public void update(double delta) {
      super.update(delta);

      if(jumping) {
        if(getY() > maxY) {
          setY((float)(getY() - jumpingSpeed * delta));
        }
        else {
          jumping = false;
        }
      }
      else {
        if(getY() < minY) {
          setY((float)(getY() + jumpingSpeed * delta));
        }
        else {
          jumping = true;
        }
      }
  }

  @Override
  public void draw(Graphics g, JPanel p) {
    g.setColor(new Color(0, 255, 0));
    g.fillRect((int)(this.getX()),
               (int)(this.getY()),
               this.getWidth(),
               this.getHeight());

    super.draw(g, p);
  }
}
