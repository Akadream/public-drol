/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.entities;

import java.awt.*;
import javax.swing.JPanel;

/**
 *
 * @author Akadream
 */
public class Bird extends Enemy {
  public Bird(float x, float y, int width, int height, int colliderX, int colliderY, int colliderWidth, int colliderHeight, String filename, Directions direction) {
    super(x, y - 32, width, height, colliderX, colliderY, colliderWidth, colliderHeight, filename, direction);

    this.speed = 1.5f;
  }

  public void update(double delta) {
    super.update(delta);
  }

  public void draw(Graphics g, JPanel p) {
    g.setColor(new Color(0, 0, 255));
    g.fillRect((int)(this.getX()),
               (int)(this.getY()),
               this.getWidth(),
               this.getHeight());

    super.draw(g, p);
  }
}