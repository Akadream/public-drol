/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.entities;

import java.awt.*;
import javax.swing.JPanel;

/**
 *
 * @author Akadream
 */
public class Tile extends Entity {
  private int id;
  private boolean collide;

  public Tile(float x, float y, int width, int height, int id) {
    // Call parent constructor
    super(x, y, width, height, (int)x, (int)y, width, height);
    this.id = id;

    switch(this.id) {
      case 2:
      case 3:
      case 4:
        collide = true;
        break;
      default:
        collide = false;
        break;
    }
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getId() {
    return this.id;
  }

  public void setCollide(boolean collide) {
    this.collide = collide;
  }

  public boolean canCollide() {
    return this.collide;
  }

  // These functions can't be called because every enemies behavior are differents
  @Override
  public void draw(Graphics g, JPanel p) {}

  @Override
  public void update(double delta) {
      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }
}