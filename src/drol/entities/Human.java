/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.entities;

import drol.ui.ImageManager;
import java.awt.*;
import javax.swing.JPanel;

/**
 *
 * @author Akadream
 */
public class Human extends Entity {
  private boolean hurted;
  private Directions direction;
  private float speed;

  public Human(float x, float y, int width, int height, int colliderX, int colliderY, int colliderWidth, int colliderHeight, String filename, Directions direction) {
    super(x, y, width, height, colliderX, colliderY, colliderWidth, colliderHeight);

    ImageManager im = new ImageManager(filename);
    this.sprite = im.getSprite();
    this.speed = 1.1f;
    this.direction = direction;
  }

  public boolean isHurted() {
    return hurted;
  }

  public void setHurted(boolean h) {
    hurted = h;
  }

  public void update(double delta) {
    switch(direction) {
      case LEFT:
        if(getRight() < 0) {
          setX(646);
        }

        setX((float)(getX() - speed * delta));
        break;
      case RIGHT:
        if(getX() > 614) {
          setX(-32);
        }

        setX((float)(getX() + speed * delta));
        break;
    }
  }

  public void draw(Graphics g, JPanel p) {
    g.setColor(new Color(255, 255, 255));
    g.fillRect((int)(this.getX()),
               (int)(this.getY()),
               this.getWidth(),
               this.getHeight());
  }
}