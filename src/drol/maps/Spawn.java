/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.maps;

import drol.entities.Directions;
import drol.entities.Entity;

/**
 *
 * @author Akadream
 */
public class Spawn {
  private int x;
  private int y;
  private final Directions direction;
  private boolean humanSpawn;

  public Spawn(int x, int y, Directions direction, boolean humanSpawn) {
    this.x = x;
    this.y = y;
    this.direction = direction;
    this.humanSpawn = humanSpawn;
  }

  public void setX(int x) {
    this.x = y;
  }

  public void setY(int y) {
    this.y = y;
  }

  public int getX() {
    return this.x;
  }

  public int getY() {
    return this.y;
  }

  public Directions getDirection() {
    return this.direction;
  }

  public boolean canHumanSpawn() {
    return this.humanSpawn;
  }

  public void setHumanSpawn(boolean humanSpawn) {
    this.humanSpawn = humanSpawn;
  }

  public void spawnHere(Entity entity) {
    entity.setX(x);
    entity.setY(y);
  }
}