/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drol.maps;

import drol.entities.Entity;
import drol.entities.Tile;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Akadream
 */
public final class MapManager {
    // Size of every levels
  public static final int MAP_WIDTH = 19;
  public static final int MAP_HEIGHT = 16;

  // Array wich will contain tile information
  private final Tile[][] map;

  public MapManager() {
    map = new Tile[MAP_WIDTH][MAP_HEIGHT];
    levelInitialization(1);
  }

  public void levelInitialization(int level) {
    // Some variables needed to read a file
    FileInputStream input;
    InputStreamReader stream;
    LineNumberReader reader;

    // A list to stock temporaries tiles value
    List<Integer> list = new ArrayList<>();

    // Read a file can generate an IOException
    try {
      // Load the file
      input = new FileInputStream(new File("maps/level" + level + ".map"));
      stream = new InputStreamReader(input);
      reader = new LineNumberReader(stream);
      String line;

      // While we can read a line from the file
      while((line = reader.readLine()) != null) {
        // We get every information except space
        String[] tempValues = line.split(" ");
        // For every tiles value
        for(String s : tempValues) {
          // Add this value to the list
          list.add(Integer.parseInt(s));
        }
      }

      // Close the file reading system
      reader.close();
      stream.close();
      input.close();
    }
    catch(IOException e) {
        // Print potential error information on the console

    }

    int i = 0;
    int j = 0;

    // For every tiles ID
    for(int k = 0; k < list.size(); k++) {
      // Add this tile ID on the 2-dimensional array map
      Tile t = new Tile(i * 32f, j * 32f, 32, 32, list.get(k));
      map[i][j] = t;
      // The row have to be incremented by 1
      i++;

      // If we have a new lines to fill from our list,
      // Go to the next line of the map array
      if(i >= (MAP_WIDTH) && j <= (MAP_HEIGHT)) {
        i = 0;
        j++;
      }
    }
  }

  public int getTileID(int x, int y) {
    return map[x][y].getId();
  }

  public int getTileID(Entity entity) {
    return map[(int)entity.getX() / 32][(int)entity.getY() / 32].getId();
  }

  public Tile getTile(int x, int y) {
    return map[x][y];
  }

  public void setTileID(int x, int y, int value) {
    map[x][y].setId(value);
  }

  public void debug() {
    for(int x = 0 ; x < MAP_WIDTH ; x++) {
        for(int y = 0 ; y < MAP_HEIGHT ; y++) {
            System.out.print(map[x][y] + " ");
        }
        System.out.println("");
    }
  }
}
